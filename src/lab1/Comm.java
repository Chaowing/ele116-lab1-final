/******************************************************
 Class            : ELE116
 Term             : A2018
 Group            : 1
 Projet           : Laboratory #1
 Student(s)       : Chaowing Hua
                  : Freddy Hidalgo-Monchez
 Code(s) perm.    : HUAC24079605
                  : HIDF18068805
 Teacher          : Haythem Rehouma
 Date of creation : 2018-10-03
 Date last modif. : 2018-10-18
 
*******************************************************
 History of modifications
*******************************************************
*
  2018-10-18	Freddy Hidalgo-Monchez : Test Connection to server
  

********************************************************/
package lab1;
import java.io.*;
import java.net.*;


public class Comm {
	
	private Socket clientSocket;
	private PrintWriter out;
	private BufferedReader in;
	private String resp;
	private String hostName;
	private int portNum;
	
	public Comm(String hostName, int portNum) {
		this.portNum = portNum;
		this.hostName = hostName;
	}
	
	public void establishConnection(){
		
        try {
			// open connection with server and i/o methods
	    	clientSocket = new Socket(this.hostName, this.portNum);
	        out = new PrintWriter(clientSocket.getOutputStream(), true);
	        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        
        } catch (UnknownHostException e) {
            System.err.println("Don't know about host " + hostName);
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for the connection to " +
                hostName);
            System.exit(1);
        }
	}
	
	public void terminateConnection(){
		
		 try {
			out.println("END");
			in.close();
	        out.close();
	        clientSocket.close();
		 } catch (IOException e) {
			 
			 
		 }
	}
	
	
	public String send(String msgToServer){
		 
	  try {
            // discard first line "commande>"
        	in.readLine();
        	
        	// send and receive info
        	out.println(msgToServer);	        	
        	resp = in.readLine();
        	
        } catch (IOException e) {
            System.exit(1);
        }
            	       
        return resp;
	}
	
}




