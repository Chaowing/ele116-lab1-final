/******************************************************
 Class            : ELE116
 Term             : A2018
 Group            : 1
 Projet           : Laboratory #1
 Student(s)       : Chaowing Hua
                  : Freddy Hidalgo-Monchez
 Code(s) perm.    : HUAC24079605
                  : HIDF18068805
 Teacher          : Haythem Rehouma
 Date of creation : 2018-10-03
 Date last modif. : 2018-10-20
 
*******************************************************
 History of modifications
*******************************************************
*   
  2018-10-20	Chaowing Hua : Test of the draw all the shape


********************************************************/
package lab1;

import java.awt.Color;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

public class FormFactory {

	public	Form	f = null;
	
	public static abstract class Form{
			
		protected int numSeq;
		protected Color couleur;
		
		public Form(int numSeq) {
			
			this.numSeq = numSeq;
		}
		
		public abstract Shape draw();
		
	}
}
