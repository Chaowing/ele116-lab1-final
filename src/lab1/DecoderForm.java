/******************************************************
 Class            : ELE116
 Term             : A2018
 Group            : 1
 Projet           : Laboratory #1
 Student(s)       : Chaowing Hua
                  : Freddy Hidalgo-Monchez
 Code(s) perm.    : HUAC24079605
                  : HIDF18068805
 Teacher          : Haythem Rehouma
 Date of creation : 2018-10-03
 Date last modif. : 2018-10-22
 
*******************************************************
 History of modifications
*******************************************************
*
  2018-10-18	Freddy Hidalgo-Monchez : Test Server/decoder
  
  2018-10-22	Chaowing Hua : Set Coord to Integer


********************************************************/
package lab1;

import java.util.HashMap;


public class DecoderForm {
		
	public HashMap<String, Object> decode(String serverResponse) {
		//1 - Take in server response - 57346 <RECTANGLE> 151 217 232 283 </RECTANGLE> 
		String[] arrOfServerResponse = serverResponse.split(" ");
			
		HashMap<String, Object> mapForm = new HashMap<>();
		HashMap<String, Integer> mapFormCoord = new HashMap<>();
		
		/* 2 - Split string to elements with following pattern  */
		mapForm.put("nseq", Integer.parseInt(arrOfServerResponse[0]));
		mapForm.put("type", arrOfServerResponse[1]);
		mapForm.put("coordinates",  mapFormCoord);
		
		String coordID;
		
		// 3 - Package in nested hashmap
		for (int i = 2; i < (arrOfServerResponse.length - 1); i++) {
			coordID = "c" + (i - 1);
			mapFormCoord.put(coordID, Integer.parseInt(arrOfServerResponse[i]));
		}
	
		return mapForm;
	}
}

class Form{
	
	public static void main(String[] args) {
		
		
	}
	
}


