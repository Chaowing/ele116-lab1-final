/******************************************************
 Class            : ELE116
 Term             : A2018
 Group            : 1
 Projet           : Laboratory #1
 Student(s)       : Chaowing Hua
                  : Freddy Hidalgo-Monchez
 Code(s) perm.    : HUAC24079605
                  : HIDF18068805
 Teacher          : Haythem Rehouma
 Date of creation : 2018-10-03
 Date last modif. : 2018-10-22
 
*******************************************************
 History of modifications
*******************************************************
* 
  2018-10-17	Chaowing Hua : Test of the draw all the shape
  
  2018-10-20	Chaowing Hua : Add map ID
							   Add map Shape
							   Finalize all functions
  
  2018-10-20	Chaowing Hua : Test of the draw all the shape
  
  2018-10-22	Chaowing Hua : Add map color
  							   Change to Add() to fit decoder
  

********************************************************/

package lab1;

import java.util.ArrayList;
import java.awt.Color;
import java.awt.Shape;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import lab1.FormFactory;


public class TabForms {
	
	ArrayList <Map> listFormToDraw = new ArrayList<Map>(10);
	
	public void add(String typeForm, int id, int x1, int y1, int x2, int y2) {
		Map map = new HashMap();
		Shape shapeToDraw = null;
		
		// Color generator
		Random rand = new Random();
		float r = rand.nextFloat();
		float g = rand.nextFloat();
		float b = rand.nextFloat();
		Color newColor = new Color(r, g, b);
			
		switch (typeForm) {
			case "<CARRE>" :
				Square square= new Square(id, x1, y1, x2, y2);	
				shapeToDraw = square.draw();
				break;
			case "<CERCLE>" :
				Circle circle= new Circle(id, x1, y1, x2);	
				shapeToDraw = circle.draw();
				break;
			case "<OVALE>" :
				Ellipse ellipse = new Ellipse(id, x1, y1, x2, y2);	
				shapeToDraw = ellipse.draw();
				break;
			case "<RECTANGLE>" :
				Rectangle rectangle= new Rectangle(id, x1, y1, x2, y2);	
				shapeToDraw = rectangle.draw();
				break;
			case "<LIGNE>" :
				Line line = new Line(id, x1, y1, x2, y2);
				shapeToDraw = line.draw();
				break;
		}
				
		if (getSize() >= 10){
			remove(0);
		};
		map.put("shape",shapeToDraw);
		map.put("ID",id);
		map.put("color", newColor);
		listFormToDraw.add(map);
	}
	public void remove(int index) {
        
		listFormToDraw.remove(index);
	}
	public int getSize() {
        
		return listFormToDraw.size();
	}
	public Shape getForm(int index) {	
		
		return (Shape)listFormToDraw.get(index).get("shape");
	}
	public int getID(int index) {
        
		return (int)listFormToDraw.get(index).get("ID");
	}
	public Color getColor(int index){
		
		return (Color)listFormToDraw.get(index).get("color");
	}
}
