/******************************************************
 Class            : ELE116
 Term             : A2018
 Group            : 1
 Projet           : Laboratory #1
 Student(s)       : Chaowing Hua
                  : Freddy Hidalgo-Monchez
 Code(s) perm.    : HUAC24079605
                  : HIDF18068805
 Teacher          : Haythem Rehouma
 Date of creation : 2018-10-17
 Date last modif. : 2018-10-22
 
*******************************************************
 History of modifications
*******************************************************
* 
  2018-10-17	Chaowing Hua : Test of the shape

  2018-10-22	Chaowing Hua : Change coord to match the server

********************************************************/

package lab1;

import java.awt.Shape;
import java.awt.geom.Line2D;

import lab1.FormFactory.Form;

public class Line extends Form {
	
	public double canevasWidth;
	public double canevasHeigh;
	public double formWidth;
	public double formHeigh;
	public Line(int numSeq, int canevasWidth, int canevasHeigh, int formWidth, int formHeigh)
	{
		super(numSeq);
		this.canevasWidth = (double)canevasWidth;
		this.canevasHeigh = (double)canevasHeigh;	
		this.formWidth = (double)formWidth;
		this.formHeigh = (double)formHeigh;

	}
	public Shape draw() {
		return new Line2D.Double(this.canevasWidth, this.canevasHeigh, this.formWidth, this.formHeigh);
	}
}
