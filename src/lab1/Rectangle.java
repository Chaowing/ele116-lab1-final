/******************************************************
 Class            : ELE116
 Term             : A2018
 Group            : 1
 Projet           : Laboratory #1
 Student(s)       : Chaowing Hua
                  : Freddy Hidalgo-Monchez
 Code(s) perm.    : HUAC24079605
                  : HIDF18068805
 Teacher          : Haythem Rehouma
 Date of creation : 2018-10-15
 Date last modif. : 2018-10-22
 
*******************************************************
 History of modifications
*******************************************************
* 
  2018-10-17	Chaowing Hua : Test of the shape

  2018-10-22	Chaowing Hua : Change coord to match the server

********************************************************/

package lab1;
import java.awt.Shape;
import java.awt.geom.Rectangle2D;
import lab1.FormFactory.Form;

public class Rectangle extends Form{
	public double canevasWidth;
	public double canevasHeigh;
	public double formWidth;
	public double formHeigh;
	
	public Rectangle(int numSeq, int x1, int y1, int x2, int y2)
	{
		super(numSeq);
		this.canevasWidth = (double)x1;
		this.canevasHeigh = (double)y2;	
		this.formWidth =(double)x2-x1;
		this.formHeigh =(double)y2-y1;
		
	}
	public Shape draw() {
		return new Rectangle2D.Double(this.canevasWidth, this.canevasHeigh, this.formWidth, this.formHeigh);
	}	
	
	
}