/******************************************************
 Class            : ELE116
 Term             : A2018
 Group            : 1
 Projet           : Laboratory #1
 Student(s)       : Chaowing Hua
                  : Freddy Hidalgo-Monchez
 Code(s) perm.    : HUAC24079605
                  : HIDF18068805
 Teacher          : Haythem Rehouma
 Date of creation : 2018-10-03
 Date last modif. : 2018-10-18
 
*******************************************************
 History of modifications
*******************************************************
*
  2018-10-18	Freddy Hidalgo-Monchez : Test Server

********************************************************/

package lab1;

public class CommForm{
	
	private String host = "localhost";
	private int port = 10000;
	private Comm server;
	
	public CommForm() {
		server = new Comm(host, port);
		server.establishConnection();
	}
	
	public String sendGet() {
		return server.send("GET");
	}
	
	public void endGet() {
		server.terminateConnection();
	}
}
